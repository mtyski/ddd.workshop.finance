﻿using System;
using System.Collections.Generic;
using System.Linq;

using Ddd.Workshop.Finance.DomainModel.Aggregate.Account;
using Ddd.Workshop.Finance.DomainModel.DomainEvents.Account;
using Ddd.Workshop.Finance.DomainModel.DomainService;
using Ddd.Workshop.Finance.DomainModel.Factory;
using Ddd.Workshop.Finance.DomainModel.Repository;
using Ddd.Workshop.Finance.Framework.DomainEvent;

using Moq;

using Xunit;

namespace Ddd.Workshop.Finance.DomainModel.Tests.DomainServices
{
    [Trait("Category", "AccountRegistrationService unit tests")]
    public class AccountRegistrationServiceTests : IDisposable
    {
        private ICollection<Account> _savedAccounts;

        private Mock<IAccountRepository> _accountRepositoryMock;

        private AccountRegistrationService _service;

        private List<IDomainEvent> _emittedEvents;

        public AccountRegistrationServiceTests()
        {
            _emittedEvents = new List<IDomainEvent>();
            _savedAccounts = new List<Account>();
            _accountRepositoryMock = new Mock<IAccountRepository>();
            _accountRepositoryMock.Setup(repo => repo.PublishEventsAsync()).Callback(EmitEvents);
            _accountRepositoryMock.Setup(repo => repo.Save(It.IsAny<Account>()))
                .Callback((Account newAccount) => SaveAndSetId(newAccount));

            _service = new AccountRegistrationService(_accountRepositoryMock.Object, new AccountFactory());
        }

        public void Dispose()
        {
            _emittedEvents = null;
            _savedAccounts = null;
            _accountRepositoryMock = null;
            _service = null;
        }

        [Fact]
        public void AccountRegistrationService_OnReceivingRequest_CreatesAccountForCustomer()
        {
            _service.RegisterAccountForCustomer(1);

            Assert.Equal(1, _savedAccounts.Count);
            var savedAccount = _savedAccounts.First();
            Assert.Equal(1, savedAccount.Id);
            Assert.False(savedAccount.IsActive());

            Assert.Empty(savedAccount.GetDomainEvents());
            Assert.Single(_emittedEvents);
            Assert.IsType<NewAccountRegistered>(_emittedEvents.First());
            var emittedNewAccountRegisteredEvent = (NewAccountRegistered)_emittedEvents.First();
            Assert.True(emittedNewAccountRegisteredEvent.CustomerId == 1 && emittedNewAccountRegisteredEvent.AccountId == 1);
            Assert.NotEqual(default(DateTime), emittedNewAccountRegisteredEvent.CreatedOn);
        }

        private void EmitEvents()
        {
            var accountsWithEvents = _savedAccounts.Where(a => a.GetDomainEvents().Any());
            foreach (var account in accountsWithEvents)
            {
                _emittedEvents.AddRange(account.GetDomainEvents());
                account.ClearDomainEvents();
            }
        }

        private void SaveAndSetId(Account newAccount)
        {
            _savedAccounts.Add(newAccount);
            newAccount.Id = _savedAccounts.Count;
            newAccount.AddDomainEvent(new NewAccountRegistered(newAccount.Id, newAccount.CustomerId));
            _accountRepositoryMock.Object.PublishEventsAsync();
        }
    }
}