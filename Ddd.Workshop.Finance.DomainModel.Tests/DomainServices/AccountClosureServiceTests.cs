﻿using System;
using System.Collections.Generic;
using System.Linq;

using Ddd.Workshop.Finance.DomainModel.Aggregate.Account;
using Ddd.Workshop.Finance.DomainModel.Aggregate.Account.Exceptions;
using Ddd.Workshop.Finance.DomainModel.Aggregate.Customer;
using Ddd.Workshop.Finance.DomainModel.DomainEvents.Account;
using Ddd.Workshop.Finance.DomainModel.DomainService;
using Ddd.Workshop.Finance.DomainModel.Factory;
using Ddd.Workshop.Finance.DomainModel.Repository;
using Ddd.Workshop.Finance.DomainModel.Saga.Account;
using Ddd.Workshop.Finance.DomainModel.ValueObject.Account.Exceptions;
using Ddd.Workshop.Finance.Framework.DomainEvent;

using Moq;

using Xunit;

namespace Ddd.Workshop.Finance.DomainModel.Tests.DomainServices
{
    [Trait("Category", "AccountClosureService unit tests")]
    public class AccountClosureServiceTests : IDisposable
    {
        private Mock<IAccountRepository> _accountRepositoryMock;

        private List<Account> _accounts;

        private AccountClosureService _accountClosureService;

        private ICollection<IDomainEvent> _emittedEvents;

        public AccountClosureServiceTests()
        {
            _accountRepositoryMock = new Mock<IAccountRepository>();

            _accountRepositoryMock.Setup(repo => repo.Get(It.IsAny<long>()))
                .Returns((long id) => _accounts.FirstOrDefault(account => account.Id == id));
            _accountRepositoryMock.Setup(repo => repo.Save(It.IsAny<Account>()))
                .Callback((Account newAccount) => SaveNewAccount(newAccount));
            _accountRepositoryMock.Setup(repo => repo.Update(It.IsAny<Account>()))
                .Callback((Account updatedAccount) => UpdateAccounts(updatedAccount));
            _accountRepositoryMock.Setup(repo => repo.PublishEventsAsync())
                .Callback(EmitAllDomainEventsFromRepository);

            SetupAccounts();

            _accountClosureService = new AccountClosureService(_accountRepositoryMock.Object);
        }

        public void Dispose()
        {
            _accountRepositoryMock = null;
            _accounts = null;
            _accountClosureService = null;
            _emittedEvents = null;
        }

        [Fact]
        public void AccountClosureService_OnReceivingRequest_ClosesAccount()
        {
            _accountClosureService.CloseAccount(1);

            var account = _accountRepositoryMock.Object.Get(1);
            Assert.True(account.IsClosed());
            Assert.Single(_emittedEvents);
            Assert.True(_emittedEvents.First() is AccountClosed);
            var emittedEvent = (AccountClosed)_emittedEvents.First();
            Assert.Equal(1, emittedEvent.AccountId);
            Assert.NotEqual(default(DateTime), emittedEvent.CreatedOn);
        }

        [Fact]
        public void AccountClosureService_OnReceivingRequest_ThrowsExceptionIfAccountIsAlreadyClosed()
        {
            _accountClosureService.CloseAccount(1);

            Assert.Throws<AccountIsClosedException>(() => _accountClosureService.CloseAccount(1));
        }

        [Fact]
        public void AccountClosureService_OnReceivingRequest_ThrowsExceptionIfAccountHasNonZeroBalance()
        {
            var accountToClose = _accounts.First(a => a.IsActive());
            accountToClose.Deposit(1);

            Assert.Throws<AccountBalanceIsNotZeroException>(() => _accountClosureService.CloseAccount(accountToClose.Id));
        }

        [Fact]
        public void AccountClosureService_OnReceivingRequest_ThrowsExceptionIfAccountIsNew()
        {
            var accountToClose = new AccountFactory().CreateAccount();
            _accounts.Add(accountToClose);
            accountToClose.Id = _accounts.Count;

            Assert.Throws<AccountIsNotActiveException>(() => _accountClosureService.CloseAccount(accountToClose.Id));
        }

        private void EmitAllDomainEventsFromRepository()
        {
            _emittedEvents = _accounts.SelectMany(account => account.GetDomainEvents()).ToArray();

            foreach (var account in _accounts)
                account.ClearDomainEvents();
        }

        private void SaveNewAccount(Account newAccount)
        {
            _accounts.Add(newAccount);
            newAccount.Id = _accounts.Count;
        }

        private void SetupAccounts()
        {
            _accounts = new List<Account>();

            var service = new AccountRegistrationService(_accountRepositoryMock.Object, new AccountFactory());
            service.RegisterAccountForCustomer(1);

            var customers = new List<Customer>();
            var newCustomer = new CustomerFactory().CreateCustomer("Name", "Surname");
            newCustomer.Id = 1;
            customers.Add(newCustomer);

            var customerRepositoryMock = new Mock<ICustomerRepository>();
            customerRepositoryMock.Setup(repo => repo.Update(It.IsAny<Customer>()));
            customerRepositoryMock.Setup(repo => repo.Get(It.IsAny<long>()))
                .Returns((long id) => customers.FirstOrDefault(customer => customer.Id.Equals(id)));
            customerRepositoryMock.Setup(repo => repo.PublishEventsAsync());

            var saga = new NewAccountRegisteredSaga(customerRepositoryMock.Object, _accountRepositoryMock.Object);
            saga.Handle(new NewAccountRegistered(1, newCustomer.Id));
        }

        private void UpdateAccounts(Account updatedAccount)
        {
            _accounts.Remove(updatedAccount);
            _accounts.Add(updatedAccount);
            updatedAccount.AddDomainEvent(new AccountClosed(updatedAccount.Id));
            _accountRepositoryMock.Object.PublishEventsAsync();
        }
    }
}