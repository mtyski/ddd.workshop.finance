﻿using System;

using Ddd.Workshop.Finance.DomainModel.Aggregate.Customer.Exceptions;
using Ddd.Workshop.Finance.DomainModel.Factory;

using Xunit;

namespace Ddd.Workshop.Finance.DomainModel.Tests.Factories
{
    [Trait("Category", "CustomerFactory unit tests")]
    public class CustomerFactoryTests : IDisposable
    {
        private const string FirstName = "Name";

        private const string Surname = "Surname";

        private const string MiddleName = "MiddleName";

        private const string PeselNumber = "12345678912345";

        private CustomerFactory _factory;

        public CustomerFactoryTests() => _factory = new CustomerFactory();

        public void Dispose() => _factory = null;

        [Fact]
        public void CustomerFactory_OnPassingValidCustomerData_ReturnsNewCustomerWithMiddleName()
        {
            var result = _factory.CreateCustomer(FirstName, Surname, MiddleName, PeselNumber);

            Assert.NotNull(result);
            Assert.Equal($"{FirstName} {MiddleName} {Surname}", result.GetName());
            Assert.Equal(PeselNumber, result.GetPeselNumber());
        }

        [Fact]
        public void CustomerFactory_OnPassingValidCustomerData_ReturnsNewCustomerWithNoMiddleName()
        {
            var result = _factory.CreateCustomer(FirstName, Surname, null, PeselNumber);

            Assert.NotNull(result);
            Assert.Equal($"{FirstName} {Surname}", result.GetName());
            Assert.Equal(PeselNumber, result.GetPeselNumber());
        }

        [Fact]
        public void CustomerFactory_OnPassingNullOrWhiteSpaceFirstName_ThrowsException() =>
            Assert.Throws<CustomerCannotHaveNullOrWhiteSpaceNameException>(() => _factory.CreateCustomer(null, Surname));

        [Fact]
        public void CustomerFactory_OnPassingNullOrWhiteSpaceSurname_ThrowsException() =>
            Assert.Throws<CustomerCannotHaveNullOrWhiteSpaceSurnameException>(() => _factory.CreateCustomer(FirstName, null));
    }
}