﻿using System;

using Ddd.Workshop.Finance.DomainModel.Factory;

using Xunit;

namespace Ddd.Workshop.Finance.DomainModel.Tests.Factories
{
    [Trait("Category", "AccountFactory unit tests")]
    public class AccountFactoryTests : IDisposable
    {
        private AccountFactory _factory;

        public AccountFactoryTests() => _factory = new AccountFactory();

        [Fact]
        public void AccountFactory_OnCreatingNewAccount_CreatesValidAccount()
        {
            var result = _factory.CreateAccount();

            Assert.NotNull(result);
            Assert.False(result.IsActive());
            Assert.False(result.IsClosed());
            Assert.Empty(result.GetTransactionHistoryData());
            Assert.Equal(0, result.GetAccountBalance());
        }

        public void Dispose() => _factory = null;
    }
}
