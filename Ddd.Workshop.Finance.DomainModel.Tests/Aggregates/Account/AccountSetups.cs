﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

using Ddd.Workshop.Finance.Framework.Entity;

namespace Ddd.Workshop.Finance.DomainModel.Tests.Aggregates.Account
{
    internal static class AccountSetups
    {
        public static void PersistTransactions(this Aggregate.Account.Account account)
        {
            var transactions =
                (IEnumerable<Entity<Guid>>)account.GetType()
                    .GetField("_transactionHistory", BindingFlags.Instance | BindingFlags.NonPublic)
                    .GetValue(account);
            foreach (var transaction in transactions.Where(t => t.Id == Guid.Empty))
                transaction.Id = Guid.NewGuid();
        }
    }
}