﻿using System;
using System.Collections.Generic;
using System.Linq;

using Ddd.Workshop.Finance.DomainModel.Aggregate.Account.Exceptions;
using Ddd.Workshop.Finance.DomainModel.Aggregate.Customer;
using Ddd.Workshop.Finance.DomainModel.DomainEvents.Account;
using Ddd.Workshop.Finance.DomainModel.DomainService;
using Ddd.Workshop.Finance.DomainModel.Entity.Account.Data;
using Ddd.Workshop.Finance.DomainModel.Factory;
using Ddd.Workshop.Finance.DomainModel.Repository;
using Ddd.Workshop.Finance.DomainModel.Saga.Account;
using Ddd.Workshop.Finance.DomainModel.ValueObject.Account.Exceptions;

using Moq;

using Xunit;
using Xunit.Sdk;

namespace Ddd.Workshop.Finance.DomainModel.Tests.Aggregates.Account
{
    [Trait("Category", "Account unit tests")]
    public class AccountTests : IDisposable
    {
        private Aggregate.Account.Account _newAccount;

        private Aggregate.Account.Account _activeAccount;

        private Aggregate.Account.Account _closedAccount;

        private ICollection<Aggregate.Account.Account> _accounts;

        public AccountTests()
        {
            _accounts = new List<Aggregate.Account.Account>();
            SetupAccounts();
        }

        public void Dispose()
        {
            _newAccount = null;
            _activeAccount = null;
            _closedAccount = null;
            _accounts = null;
        }

        [Fact]
        public void ActiveAccount_OnRegisteringDeposit_IncreasesBalance()
        {
            const decimal DepositAmount = (decimal)100.00;

            _activeAccount.Deposit(DepositAmount);

            Assert.Equal(DepositAmount, _activeAccount.GetAccountBalance());
        }

        [Fact]
        public void ActiveAccount_OnRegisteringWithdrawal_DecreasesBalance()
        {
            const decimal TransactionAmount = (decimal)3.50;
            _activeAccount.Deposit(TransactionAmount);

            _activeAccount.Withdraw(TransactionAmount);

            Assert.Equal(0, _activeAccount.GetAccountBalance());
        }

        [Fact]
        public void ActiveAccount_OnRegisteringWithdrawal_ThrowsExceptionIfWithdrawalExceedsBalance()
        {
            const decimal TransacionAmount = decimal.One;
            Assert.Throws<WithdrawalAmountExceedsAccountBalanceException>(
                () => _activeAccount.Withdraw(TransacionAmount));
        }

        [Fact]
        public void NewAccount_OnRegisteringDeposit_ThrowsException() =>
            Assert.Throws<AccountIsNotActiveException>(() => _newAccount.Deposit(decimal.One));

        [Fact]
        public void NewAccount_OnRegisteringWithdrawal_ThrowsException() =>
            Assert.Throws<AccountIsNotActiveException>(() => _newAccount.Withdraw(decimal.One));

        [Fact]
        public void ClosedAccount_OnRegisteringDeposit_ThrowsException() =>
            Assert.Throws<AccountIsClosedException>(() => _closedAccount.Deposit(decimal.One));

        [Fact]
        public void ClosedAccount_OnRegisteringWithdrawal_ThrowsException() =>
            Assert.Throws<AccountIsClosedException>(() => _closedAccount.Withdraw(decimal.One));

        [Fact]
        public void ActiveAccount_OnRegisteringDeposit_CreatesNewTransientTransaction()
        {
            _activeAccount.Deposit(decimal.One);

            var transactionHistory = _activeAccount.GetTransactionHistoryData().ToList();
            Assert.Single(transactionHistory);
            var transaction = transactionHistory.First();
            Assert.Equal(decimal.One, transaction.Amount);
            Assert.Equal(TransactionData.TransactionType.Deposit, transaction.Type);
            Assert.Equal(Guid.Empty, transaction.TransactionId);
        }

        [Fact]
        public void ActiveAccount_OnRegisteringValidWithdrawal_CreatesNewTransientTransaction()
        {
            const decimal TransactionAmount = decimal.One;
            _activeAccount.Deposit(TransactionAmount);
            _activeAccount.PersistTransactions();
            _activeAccount.Withdraw(TransactionAmount);

            var transactionHistory = _activeAccount.GetTransactionHistoryData().ToList();
            Assert.Equal(2, transactionHistory.Count);
            var withdrawal =
                transactionHistory.FirstOrDefault(td => td.Type == TransactionData.TransactionType.Withdrawal) ??
                throw new XunitException("No withdrawal was registered!");
            Assert.Equal(TransactionAmount, withdrawal.Amount);
            Assert.Equal(Guid.Empty, withdrawal.TransactionId);
        }

        private void SetupAccounts()
        {
            var accountFactory = new AccountFactory();
            SetupNewAccount(accountFactory);

            var accountRepoMock = SetupRepositoryMockForServices();

            SetupActiveAccount(accountRepoMock, accountFactory);

            SetupClosedAccount(accountRepoMock);
        }

        private void SetupClosedAccount(IMock<IAccountRepository> accountRepoMock)
        {
            var accountClosureService = new AccountClosureService(accountRepoMock.Object);
            accountClosureService.CloseAccount(_accounts.Last(a => a.IsActive()).Id);
            _closedAccount = _accounts.First(a => a.IsClosed());
        }

        private void SetupActiveAccount(IMock<IAccountRepository> accountRepositoryMock, AccountFactory accountFactory)
        {
            var registrationService = new AccountRegistrationService(accountRepositoryMock.Object, accountFactory);
            for (var i = 0; i < 2; i++)
                registrationService.RegisterAccountForCustomer(1);

            var customers = new List<Customer>();
            var newCustomer = new CustomerFactory().CreateCustomer("Name", "Surname");
            newCustomer.Id = 1;
            customers.Add(newCustomer);

            var customerRepositoryMock = new Mock<ICustomerRepository>();
            customerRepositoryMock.Setup(repo => repo.Update(It.IsAny<Customer>()));
            customerRepositoryMock.Setup(repo => repo.Get(It.IsAny<long>()))
                .Returns((long id) => customers.FirstOrDefault(customer => customer.Id.Equals(id)));
            customerRepositoryMock.Setup(repo => repo.PublishEventsAsync());

            var saga = new NewAccountRegisteredSaga(customerRepositoryMock.Object, accountRepositoryMock.Object);

            for (var i = 0; i < 2; i++)
                saga.Handle(new NewAccountRegistered(i + 2, 1));

            _activeAccount = _accounts.First(a => a.IsActive());
        }

        private void UpdateAccount(Aggregate.Account.Account updatedAccount)
        {
            _accounts.Remove(updatedAccount);
            _accounts.Add(updatedAccount);
        }

        private void SaveNewAccount(Aggregate.Account.Account newAccount)
        {
            _accounts.Add(newAccount);
            newAccount.Id = _accounts.Count;
        }

        private void SetupNewAccount(AccountFactory accountFactory)
        {
            _newAccount = accountFactory.CreateAccount();
            SaveNewAccount(_newAccount);
        }

        private Mock<IAccountRepository> SetupRepositoryMockForServices()
        {
            var accountRepositoryMock = new Mock<IAccountRepository>();
            accountRepositoryMock.Setup(repo => repo.Get(It.IsAny<long>()))
                .Returns((long id) => _accounts.FirstOrDefault(account => account.Id.Equals(id)));
            accountRepositoryMock.Setup(repo => repo.Save(It.IsAny<Aggregate.Account.Account>()))
                .Callback((Aggregate.Account.Account newAccount) => SaveNewAccount(newAccount));
            accountRepositoryMock.Setup(repo => repo.Update(It.IsAny<Aggregate.Account.Account>()))
                .Callback((Aggregate.Account.Account updatedAccount) => UpdateAccount(updatedAccount));
            accountRepositoryMock.Setup(repo => repo.PublishEventsAsync());
            return accountRepositoryMock;
        }
    }
}