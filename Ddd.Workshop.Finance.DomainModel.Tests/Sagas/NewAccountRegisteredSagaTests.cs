﻿using System;
using System.Collections.Generic;
using System.Linq;

using Ddd.Workshop.Finance.DomainModel.Aggregate.Account;
using Ddd.Workshop.Finance.DomainModel.Aggregate.Account.Exceptions;
using Ddd.Workshop.Finance.DomainModel.Aggregate.Customer;
using Ddd.Workshop.Finance.DomainModel.DomainEvents.Account;
using Ddd.Workshop.Finance.DomainModel.DomainService;
using Ddd.Workshop.Finance.DomainModel.Factory;
using Ddd.Workshop.Finance.DomainModel.Repository;
using Ddd.Workshop.Finance.DomainModel.Saga.Account;
using Ddd.Workshop.Finance.DomainModel.Saga.Account.Exceptions;

using Moq;

using Xunit;

namespace Ddd.Workshop.Finance.DomainModel.Tests.Sagas
{
    [Trait("Category", "NewAccountRegisteredSaga unit tests")]
    public class NewAccountRegisteredSagaTests : IDisposable
    {
        private Mock<ICustomerRepository> _customerRepositoryMock;

        private Mock<IAccountRepository> _accountRepositoryMock;

        private ICollection<Customer> _customers;

        private ICollection<Account> _accounts;

        private NewAccountRegisteredSaga _saga;

        public NewAccountRegisteredSagaTests()
        {
            _customerRepositoryMock = new Mock<ICustomerRepository>();
            _customerRepositoryMock.Setup(repo => repo.Update(It.IsAny<Customer>()))
                .Callback((Customer updatedCustomer) => UpdateCustomer(updatedCustomer));
            _customerRepositoryMock.Setup(repo => repo.Get(It.IsAny<long>()))
                .Returns((long id) => _customers.FirstOrDefault(customer => customer.Id.Equals(id)));
            _customerRepositoryMock.Setup(repo => repo.PublishEventsAsync());

            _accountRepositoryMock = new Mock<IAccountRepository>();
            _accountRepositoryMock.Setup(repo => repo.Get(It.IsAny<long>()))
                .Returns((long id) => _accounts.FirstOrDefault(a => a.Id.Equals(id)));
            _accountRepositoryMock.Setup(repo => repo.Save(It.IsAny<Account>()))
                .Callback((Account newAccount) => SaveNewAccount(newAccount));
            _accountRepositoryMock.Setup(repo => repo.Update(It.IsAny<Account>()))
                .Callback((Account updatedAccount) => UpdateAccount(updatedAccount));
            _accountRepositoryMock.Setup(repo => repo.PublishEventsAsync());

            SetupCustomers();

            SetupAccounts();

            _saga = new NewAccountRegisteredSaga(_customerRepositoryMock.Object, _accountRepositoryMock.Object);
        }

        public void Dispose()
        {
            _customers = null;
            _accounts = null;
            _customerRepositoryMock = null;
            _accountRepositoryMock = null;
            _saga = null;
        }

        [Fact]
        public void NewAccountRegisteredSaga_OnReceivingEvent_AssignsAccountToACustomer()
        {
            var domainEvent = new NewAccountRegistered(1, 1);

            _saga.Handle(domainEvent);

            var testCustomer = _customerRepositoryMock.Object.Get(1);
            Assert.Equal(1, testCustomer.Accounts.Count);
            Assert.Equal(1, testCustomer.Accounts.First());
        }

        [Fact]
        public void NewAccountRegisteredSaga_OnReceivingEvent_ThrowsExceptionIfCustomerWasNotFound()
        {
            var domainEvent = new NewAccountRegistered(1, 2);

            Assert.Throws<CustomerNotFoundException>(() => _saga.Handle(domainEvent));
        }

        [Fact]
        public void NewAccountRegisteredSaga_OnRecveivingEvent_ThrowsExceptionIfAccountWasNotFound()
        {
            var domainEvent = new NewAccountRegistered(2, 1);

            Assert.Throws<AccountNotFoundException>(() => _saga.Handle(domainEvent));
        }

        [Fact]
        public void NewAccountRegisteredSaga_OnReceivingEvent_ThrowsExcepionIfAccountIsAlreadyActive()
        {
            var domainEvent = new NewAccountRegistered(1, 1);
            _saga.Handle(domainEvent);

            Assert.Throws<AccountIsAlreadyActiveException>(() => _saga.Handle(domainEvent));
        }

        [Fact]
        public void NewAccountRegisteredSaga_OnReceivingEvent_ThrowsExceptionIfAccountIsClosed()
        {
            var domainEvent = new NewAccountRegistered(1, 1);
            _saga.Handle(domainEvent);

            var closureService = new AccountClosureService(_accountRepositoryMock.Object);
            closureService.CloseAccount(1);

            Assert.Throws<AccountIsClosedException>(() => _saga.Handle(domainEvent));
        }

        private void SaveNewAccount(Account newAccount)
        {
            _accounts.Add(newAccount);
            newAccount.Id = _accounts.Count;
        }

        private void UpdateAccount(Account updatedAccount)
        {
            _accounts.Remove(updatedAccount);
            _accounts.Add(updatedAccount);
            _accountRepositoryMock.Object.PublishEventsAsync();
        }

        private void SetupCustomers()
        {
            var testCustomer = new CustomerFactory().CreateCustomer("Name", "Surname");
            testCustomer.Id = 1;

            _customers = new List<Customer> { testCustomer };
        }

        private void SetupAccounts()
        {
            _accounts = new List<Account>();
            var registrationService = new AccountRegistrationService(
                _accountRepositoryMock.Object,
                new AccountFactory());

            registrationService.RegisterAccountForCustomer(_customers.First().Id);
        }

        private void UpdateCustomer(Customer updatedCustomer)
        {
            _customers.Remove(updatedCustomer);
            _customers.Add(updatedCustomer);
            _customerRepositoryMock.Object.PublishEventsAsync();
        }
    }
}