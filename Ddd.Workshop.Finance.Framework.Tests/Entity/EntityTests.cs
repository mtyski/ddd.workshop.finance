﻿using System;
using System.Linq;

using Ddd.Workshop.Finance.Framework.DomainEvent;
using Ddd.Workshop.Finance.Framework.Entity;

using Xunit;

namespace Ddd.Workshop.Finance.Framework.Tests.Entity
{
    [Trait("Category", EntityTestsCategory)]
    public class EntityTests : IDisposable
    {
        private const string EntityTestsCategory = "Entity unit tests";

        private TestEntity _testEntity1;

        private TestEntity _testEntity2;

        public EntityTests()
        {
            _testEntity1 = new TestEntity();
            _testEntity2 = new TestEntity();
        }

        [Fact]
        public void TestEntity_OnGettingId_ReturnsDefaultWhenIdIsNotSet()
        {
            // Arrange - constructor
            // Act, Assert
            Assert.Equal(default(long), _testEntity1.Id);
        }

        [Fact]
        public void TestEntity_OnGettingId_ReturnsPreviouslySetId()
        {
            // Arrange
            _testEntity1.Id = 1;

            // Act, Assert
            Assert.Equal(1, _testEntity1.Id);
        }

        [Fact]
        public void TestEntity_OnCallingEquals_ReturnsTrueIfOtherEntityHasTheSameId()
        {
            // Arrange - constructor
            // Act, Assert
            Assert.Equal(_testEntity1, _testEntity2);
        }

        [Fact]
        public void TestEntity_OnCallingEquals_ReturnsTrueIfReferenceEquals()
        {
            // Arrange
            var obj = _testEntity1;

            // Act, Assert
            Assert.True(obj.Equals(_testEntity1));
        }

        [Fact]
        public void TestEntity_OnCallingEquals_ReturnsFalseIfOtherEntityHasDifferentId()
        {
            // Arrange
            _testEntity2.Id = 1;

            // Act, Assert
            Assert.False(_testEntity1.Equals(_testEntity2));
        }

        [Fact]
        public void TestEntity_OnCallingEquals_ReturnsFalseIfNonNullEntityIsComparedToNull()
        {
            // Arrange - constructor
            // Act, Assert
            Assert.False(_testEntity1.Equals(null));
        }

        [Fact]
        public void TestEntity_OnCallingEquals_ReturnsFalseIfTypesDoNotMatch()
        {
            // Arrange
            var otherEntity = new OtherTestEntity();

            // Act, Assert
            // ReSharper disable once SuspiciousTypeConversion.Global
            Assert.False(_testEntity1.Equals(otherEntity));
        }

        [Fact]
        public void TestEntity_OnUsingEqualityOperator_ReturnsTrueIfNullEntitiesAreCompared()
        {
            // Arrange
            _testEntity1 = null;
            _testEntity2 = null;

            // Act, Assert
            Assert.True(_testEntity1 == _testEntity2);
        }

        [Fact]
        public void TestEntity_OnUsingEqualityOperator_ReturnsFalseIfNullEntityIsComparedWithNonNullOne()
        {
            // Arrange
            TestEntity testEntity = null;

            // Act, Assert
            Assert.False(_testEntity1 == testEntity);
        }

        [Fact]
        public void TestEntity_OnUsingEqualityOperator_ReturnsTrueForEntitiesWithEqualIds()
        {
            // Arrange
            _testEntity1.Id = 1;
            _testEntity2.Id = _testEntity1.Id;

            // Act, Assert
            Assert.True(_testEntity1 == _testEntity2);
        }

        [Fact]
        public void TestEntity_OnUsingInequalityOperator_ReturnsFalseIfEntitiesAreEqual()
        {
            // Arrange - constructor
            // Act, Assert
            Assert.False(_testEntity1 != _testEntity2);
        }

        [Fact]
        public void TestEntity_OnAddingNewDomainEvent_StoresNewEvent()
        {
            // Arrange
            var domainEvent = new TestDomainEvent(1);

            // Act
            _testEntity1.AddDomainEvent(domainEvent);

            // Assert
            var domainEvents = _testEntity1.GetDomainEvents();
            Assert.Equal(1, domainEvents.Count);
            Assert.Equal(1, ((TestDomainEvent)domainEvents.First()).Id);
        }

        [Fact]
        public void TestEntity_OnClearingDomainEvents_ClearsDomainEventsCollection()
        {
            // Arrange
            _testEntity1.AddDomainEvent(new TestDomainEvent(1));

            // Act
            _testEntity1.ClearDomainEvents();

            // Assert
            Assert.True(_testEntity1.GetDomainEvents().Count == 0);
        }

        [Fact]
        public void TestEntity_OnGettingHashCode_ReturnsSameHashCodeForEqualEntities()
        {
            // Arrange - constructor
            // Act, Assert
            Assert.Equal(_testEntity1.GetHashCode(), _testEntity2.GetHashCode());
        }

        [Fact]
        public void TestEntity_OnGettingHashCode_ReturnsDifferentHashCodeForNonEqualEntities()
        {
            // Arrange
            _testEntity1.Id = 1;

            //Act, Assert
            Assert.NotEqual(_testEntity1.GetHashCode(), _testEntity2.GetHashCode());
        }

        public void Dispose()
        {
            _testEntity1 = null;
            _testEntity2 = null;
        }

        private class TestEntity : Entity<long>
        {
        }

        private class OtherTestEntity : Entity<long>
        {
        }

        private class TestDomainEvent : IDomainEvent
        {
            public int Id { get; }

            public TestDomainEvent(int id) => Id = id;
        }
    }
}