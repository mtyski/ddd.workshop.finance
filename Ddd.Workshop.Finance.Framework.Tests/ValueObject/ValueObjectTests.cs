using System;

using Ddd.Workshop.Finance.Framework.ValueObject;

using Xunit;

namespace Ddd.Workshop.Finance.Framework.Tests.ValueObject
{
    [Trait("Category", ValueObjectTestCategory)]
    public class ValueObjectTests : IDisposable
    {
        private const string ValueObjectTestCategory = "ValueObject unit tests";

        private Amount _amount1;

        private Amount _amount2;

        public ValueObjectTests()
        {
            _amount1 = Amount.FromInteger(0);
            _amount2 = Amount.FromInteger(0);
        }

        public void Dispose()
        {
            _amount1 = null;
            _amount2 = null;
        }

        [Fact]
        public void Amount_FromAnyInteger_ReturnsValidAmount()
        {
            // Arrange, Act - constructor
            // Assert
            Assert.NotNull(_amount1);
        }

        [Fact]
        public void Amounts_FromEqualIntegers_AreEqual()
        {
            // Arrange, Act - constructor
            // Act, Assert
            Assert.Equal(_amount1, _amount2);
        }

        [Fact]
        public void Amounts_FromEqualIntegers_HaveTheSameHashCodes()
        {
            // Arrange - constructor
            // Act, Assert
            Assert.Equal(_amount1.GetHashCode(), _amount2.GetHashCode());
        }

        [Fact]
        public void Amount_WhenCalledEqualsWithOtherObjectType_ShouldReturnFalse()
        {
            // Arrange
            const int Amount2 = 0;

            // Act, Assert
            Assert.NotEqual<object>(_amount1, Amount2);
        }

        [Fact]
        public void Amount_WhenComparedUsingEqualityOperator_ShouldReturnTrueForComparingTheSameObject()
        {
            // Arrange - constructor
            // Act, Assert
            // ReSharper disable once EqualExpressionComparison
            Assert.True(_amount1 == _amount1);
        }

        [Fact]
        public void Amount_WhenComparedToNull_ReturnsTrueIfItIsNull()
        {
            // Arrange
            Amount amount = null;

            // Act, Assert
            Assert.True(amount == null);
        }

        [Fact]
        public void NonNullAmount_WhenComparedToNull_ReturnsFalse()
        {
            // Arrange - constructor
            // Act, Assert
            Assert.False(_amount1 == null);
        }

        [Fact]
        public void NonAmount_WhenComparedToNullViaInequality_ReturnsTrue()
        {
            // Arrange - constructor
            // Act, Assert
            Assert.True(_amount1 != null);
        }

        [Fact]
        public void NullAmount_WhenComparedToNullViaInequality_ReturnsFalse()
        {
            // Arrange
            Amount amount = null;

            // Act, Assert
            Assert.False(amount != null);
        }

        private class Amount : ValueObject<Amount>
        {
            private readonly int _amount;

            private Amount(int amount) => _amount = amount;

            public static Amount FromInteger(int amount) => new Amount(amount);

            protected override bool InternalEquals(Amount otherValueObject) => _amount == otherValueObject._amount;

            protected override int InternalGetHashCode() => _amount.GetHashCode();
        }
    }
}