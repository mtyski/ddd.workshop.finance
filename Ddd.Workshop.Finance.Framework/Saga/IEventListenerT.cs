﻿using Ddd.Workshop.Finance.Framework.DomainEvent;

namespace Ddd.Workshop.Finance.Framework.Saga
{
    public interface IEventListener<in TEvent>
        where TEvent : IDomainEvent
    {
        void Handle(TEvent domainEvent);
    }
}