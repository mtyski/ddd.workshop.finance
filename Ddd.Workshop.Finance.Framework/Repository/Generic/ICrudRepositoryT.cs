﻿using Ddd.Workshop.Finance.Framework.Aggregate;

namespace Ddd.Workshop.Finance.Framework.Repository.Generic
{
    public interface ICrudRepository<TAggregate, in TId> : IRepository
        where TAggregate : Entity.Entity<TId>, IAggregateRoot
    {
        void Save(TAggregate newAggregate);

        TAggregate Get(TId id);

        void Update(TAggregate updatedAggregate);

        void Delete(TAggregate aggregateToDelete);
    }
}