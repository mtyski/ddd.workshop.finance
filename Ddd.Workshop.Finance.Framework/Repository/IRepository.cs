﻿using System.Threading.Tasks;

namespace Ddd.Workshop.Finance.Framework.Repository
{
    public interface IRepository
    {
        Task<bool> PublishEventsAsync();
    }
}