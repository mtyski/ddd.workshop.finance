﻿using System.Diagnostics.CodeAnalysis;

namespace Ddd.Workshop.Finance.Framework.ValueObject
{
    [ExcludeFromCodeCoverage]
    public abstract class ValueObject<T>
        where T : ValueObject<T>
    {
        public static bool operator ==(ValueObject<T> firstValueObject, ValueObject<T> secondValueObject)
        {
            if (firstValueObject is null && secondValueObject is null)
                return true;

            if (firstValueObject is null || secondValueObject is null)
                return false;

            return ReferenceEquals(firstValueObject, secondValueObject) || firstValueObject.Equals(secondValueObject);
        }

        public static bool operator !=(ValueObject<T> firstValueObject, ValueObject<T> secondValueObject) => !(firstValueObject == secondValueObject);

        public override bool Equals(object obj) => obj is ValueObject<T> && InternalEquals((T)obj);

        public override int GetHashCode() => InternalGetHashCode();

        protected abstract bool InternalEquals(T otherValueObject);

        protected abstract int InternalGetHashCode();
    }
}