﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Ddd.Workshop.Finance.Framework.DomainEvent;

namespace Ddd.Workshop.Finance.Framework.Entity
{
    [ExcludeFromCodeCoverage]
    public abstract class Entity<TId>
    {
        private readonly ICollection<IDomainEvent> _domainEvents;

        protected Entity()
        {
            Id = default(TId);
            _domainEvents = new List<IDomainEvent>();
        }

        public TId Id { get; set; }

        public static bool operator ==(Entity<TId> firstEntity, Entity<TId> secondEntity)
        {
            if (firstEntity is null && secondEntity is null)
                return true;

            if (firstEntity is null || secondEntity is null)
                return false;

            return ReferenceEquals(firstEntity, secondEntity) || firstEntity.Id.Equals(secondEntity.Id);
        }

        public static bool operator !=(Entity<TId> firstEntity, Entity<TId> secondEntity) => !(firstEntity == secondEntity);

        public void AddDomainEvent(IDomainEvent domainEvent) => _domainEvents.Add(domainEvent);

        public ICollection<IDomainEvent> GetDomainEvents() => _domainEvents;

        public void ClearDomainEvents() => _domainEvents.Clear();

        public override bool Equals(object obj) => obj is Entity<TId> entity && obj.GetType() == GetType() && entity.Id.Equals(Id);

        public override int GetHashCode()
        {
            unchecked
            {
                return GetType().GetHashCode() * Id.GetHashCode();
            }
        }
    }
}