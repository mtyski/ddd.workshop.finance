﻿using Ddd.Workshop.Finance.DomainModel.ValueObject.Transaction.Exceptions;
using Ddd.Workshop.Finance.Framework.ValueObject;

namespace Ddd.Workshop.Finance.DomainModel.ValueObject.Transaction
{
    internal class Amount : ValueObject<Amount>
    {
        private readonly decimal _amount;

        private Amount(decimal amount) => _amount = amount;

        public static Amount FromDecimal(decimal amount) =>
            amount > 0 ? new Amount(amount) : throw new TransactionAmountHasToBePositiveException();

        public decimal ToDecimal() => _amount;

        protected override bool InternalEquals(Amount otherValueObject) => _amount == otherValueObject._amount;

        protected override int InternalGetHashCode()
        {
            unchecked
            {
                return GetType().GetHashCode() * _amount.GetHashCode();
            }
        }
    }
}