﻿using System;

namespace Ddd.Workshop.Finance.DomainModel.ValueObject.Transaction.Exceptions
{
    public class TransactionAmountHasToBePositiveException : Exception
    {
    }
}