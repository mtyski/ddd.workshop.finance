﻿using Ddd.Workshop.Finance.Framework.ValueObject;

namespace Ddd.Workshop.Finance.DomainModel.ValueObject.Customer
{
    internal class CustomerName : ValueObject<CustomerName>
    {
        private readonly string _firstName;

        private readonly string _surname;

        private readonly string _middleName;

        private CustomerName(string firstName, string surname, string middleName = null)
        {
            _firstName = firstName;
            _surname = surname;
            _middleName = middleName;
        }

        public static CustomerName FromArgs(string firstName, string surname, string middleName) =>
            new CustomerName(firstName, surname, middleName);

        public override string ToString() => string.IsNullOrWhiteSpace(_middleName) ? $"{_firstName} {_surname}" : $"{_firstName} {_middleName} {_surname}";

        protected override bool InternalEquals(CustomerName otherValueObject) =>
            _firstName == otherValueObject._firstName && _surname == otherValueObject._surname &&
            _middleName == otherValueObject._middleName;

        protected override int InternalGetHashCode()
        {
            unchecked
            {
                var baseHashCode = _firstName.GetHashCode() * _surname.GetHashCode();
                var middleNameHashCode = _middleName?.GetHashCode() ?? 1;
                return middleNameHashCode == 0 ? baseHashCode : baseHashCode * middleNameHashCode;
            }
        }
    }
}