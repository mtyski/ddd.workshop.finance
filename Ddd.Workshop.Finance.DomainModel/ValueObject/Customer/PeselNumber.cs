﻿using Ddd.Workshop.Finance.Framework.ValueObject;

namespace Ddd.Workshop.Finance.DomainModel.ValueObject.Customer
{
    internal class PeselNumber : ValueObject<PeselNumber>
    {
        private readonly string _peselNumber;

        private PeselNumber(string peselNumber) => _peselNumber = peselNumber;

        public static PeselNumber FromString(string pesel) => new PeselNumber(pesel);

        public override string ToString() => _peselNumber;

        protected override bool InternalEquals(PeselNumber otherValueObject) =>
            _peselNumber == otherValueObject._peselNumber;

        protected override int InternalGetHashCode() => _peselNumber.GetHashCode();
    }
}