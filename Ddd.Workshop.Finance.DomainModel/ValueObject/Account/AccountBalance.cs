﻿using System.Runtime.CompilerServices;

using Ddd.Workshop.Finance.Framework.ValueObject;

namespace Ddd.Workshop.Finance.DomainModel.ValueObject.Account
{
    internal class AccountBalance : ValueObject<AccountBalance>
    {
        private readonly decimal _amount;

        private AccountBalance(decimal amount) => _amount = amount;

        public static AccountBalance FromDecimal(decimal amount) => new AccountBalance(amount);

        public AccountBalance Deposit(decimal depositedAmount) => new AccountBalance(_amount + depositedAmount);

        public AccountBalance Withdraw(decimal withdrawnAmount) => new AccountBalance(_amount - withdrawnAmount);

        public decimal ToDecimal() => _amount;

        protected override bool InternalEquals(AccountBalance otherValueObject) => _amount == otherValueObject._amount;

        protected override int InternalGetHashCode() => _amount.GetHashCode();
    }
}