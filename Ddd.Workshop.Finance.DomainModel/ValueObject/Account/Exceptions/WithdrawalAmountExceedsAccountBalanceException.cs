﻿using System;

namespace Ddd.Workshop.Finance.DomainModel.ValueObject.Account.Exceptions
{
    public class WithdrawalAmountExceedsAccountBalanceException : Exception
    {
    }
}