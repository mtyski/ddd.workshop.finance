﻿using System;

namespace Ddd.Workshop.Finance.DomainModel.Aggregate.Customer.Exceptions
{
    public class CustomerCannotHaveNullOrWhiteSpaceNameException : Exception
    {
    }
}