﻿using System.Collections.Generic;

using Ddd.Workshop.Finance.DomainModel.ValueObject.Customer;
using Ddd.Workshop.Finance.Framework.Aggregate;
using Ddd.Workshop.Finance.Framework.Entity;

namespace Ddd.Workshop.Finance.DomainModel.Aggregate.Customer
{
    public class Customer : Entity<long>, IAggregateRoot
    {
        private readonly CustomerName _customerName;

        private readonly PeselNumber _peselNumber;

        internal Customer(string firstName, string surname, string middleName, string peselNumber)
        {
            _customerName = CustomerName.FromArgs(firstName, surname, middleName);
            _peselNumber = PeselNumber.FromString(peselNumber);
            Accounts = new List<long>();
        }

        public ICollection<long> Accounts { get; }

        public void AssignAccount(long accountId) => Accounts.Add(accountId);

        public string GetName() => _customerName.ToString();

        public string GetPeselNumber() => _peselNumber.ToString();
    }
}