﻿using Ddd.Workshop.Finance.DomainModel.Aggregate.Account.Exceptions;

namespace Ddd.Workshop.Finance.DomainModel.Aggregate.Account.AccountState.Implementation
{
    internal class New : Abstract.AccountState
    {
        public New(Account account)
            : base(account)
        {
        }

        public override Abstract.AccountState Activate() => new Active(Account);

        public override Abstract.AccountState Close() => throw new AccountIsNotActiveException();

        protected override bool InternalEquals(Abstract.AccountState otherValueObject) => otherValueObject is New;
    }
}