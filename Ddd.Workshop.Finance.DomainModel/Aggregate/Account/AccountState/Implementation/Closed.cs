﻿using Ddd.Workshop.Finance.DomainModel.Aggregate.Account.Exceptions;

namespace Ddd.Workshop.Finance.DomainModel.Aggregate.Account.AccountState.Implementation
{
    internal class Closed : Abstract.AccountState
    {
        public Closed(Account account)
            : base(account)
        {
        }

        public override Abstract.AccountState Activate() => throw new AccountIsClosedException();

        public override Abstract.AccountState Close() => throw new AccountIsClosedException();

        protected override bool InternalEquals(Abstract.AccountState otherValueObject) => otherValueObject is Closed;
    }
}