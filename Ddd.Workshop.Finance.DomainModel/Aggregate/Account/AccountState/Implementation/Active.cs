﻿using Ddd.Workshop.Finance.DomainModel.Aggregate.Account.Exceptions;

namespace Ddd.Workshop.Finance.DomainModel.Aggregate.Account.AccountState.Implementation
{
    internal class Active : Abstract.AccountState
    {
        public Active(Account account)
            : base(account)
        {
        }

        public override Abstract.AccountState Activate() => throw new AccountIsAlreadyActiveException();

        public override Abstract.AccountState Close() => new Closed(Account);

        protected override bool InternalEquals(Abstract.AccountState otherValueObject) => otherValueObject is Active;
    }
}