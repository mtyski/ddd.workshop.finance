﻿using Ddd.Workshop.Finance.Framework.ValueObject;

namespace Ddd.Workshop.Finance.DomainModel.Aggregate.Account.AccountState.Abstract
{
    internal abstract class AccountState : ValueObject<AccountState>
    {
        protected AccountState(Account account) => Account = account;

        protected Account Account { get; }

        public abstract AccountState Activate();

        public abstract AccountState Close();

        protected override int InternalGetHashCode() => Account.GetHashCode() + GetType().GetHashCode();
    }
}