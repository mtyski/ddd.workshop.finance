﻿using System;

namespace Ddd.Workshop.Finance.DomainModel.Aggregate.Account.Exceptions
{
    public class AccountIsClosedException : Exception
    {
    }
}