﻿using System.Collections.Generic;
using System.Linq;

using Ddd.Workshop.Finance.DomainModel.Aggregate.Account.AccountState.Implementation;
using Ddd.Workshop.Finance.DomainModel.Aggregate.Account.Exceptions;
using Ddd.Workshop.Finance.DomainModel.Entity.Account.Abstract;
using Ddd.Workshop.Finance.DomainModel.Entity.Account.Data;
using Ddd.Workshop.Finance.DomainModel.Entity.Account.Implementation;
using Ddd.Workshop.Finance.DomainModel.ValueObject.Account;
using Ddd.Workshop.Finance.DomainModel.ValueObject.Account.Exceptions;
using Ddd.Workshop.Finance.DomainModel.ValueObject.Transaction;
using Ddd.Workshop.Finance.Framework.Aggregate;
using Ddd.Workshop.Finance.Framework.Entity;

namespace Ddd.Workshop.Finance.DomainModel.Aggregate.Account
{
    public class Account : Entity<long>, IAggregateRoot
    {
        private readonly HashSet<Transaction> _transactionHistory;

        private AccountBalance _balance;

        private AccountState.Abstract.AccountState _state;

        internal Account()
        {
            _transactionHistory = new HashSet<Transaction>();
            _balance = AccountBalance.FromDecimal(0);
            _state = new New(this);
        }

        public long CustomerId { get; private set; }

        public void Deposit(decimal depositAmount)
        {
            if (IsClosed())
                throw new AccountIsClosedException();

            if (!IsActive())
                throw new AccountIsNotActiveException();

            _transactionHistory.Add(new Deposit(Amount.FromDecimal(depositAmount)));
            _balance = _balance.Deposit(depositAmount);
        }

        public decimal GetAccountBalance() => _balance.ToDecimal();

        public void Withdraw(decimal withdrawnAmount)
        {
            if (IsClosed())
                throw new AccountIsClosedException();

            if (!IsActive())
                throw new AccountIsNotActiveException();

            if (_balance.ToDecimal() >= withdrawnAmount)
            {
                _transactionHistory.Add(new Withdrawal(Amount.FromDecimal(withdrawnAmount)));
                _balance = _balance.Withdraw(withdrawnAmount);
            }
            else
                throw new WithdrawalAmountExceedsAccountBalanceException();
        }

        public bool IsActive() => _state is Active;

        public bool IsClosed() => _state is Closed;

        public IEnumerable<TransactionData> GetTransactionHistoryData() =>
            _transactionHistory.Select(transaction => transaction.ToTransactionData());

        internal void AssignToCustomer(long customerId)
        {
            if (_state is New)
                CustomerId = customerId;
        }

        internal void Activate() => _state = _state.Activate();

        internal void Close()
        {
            if (_balance.ToDecimal() != 0)
                throw new AccountBalanceIsNotZeroException();

            _state = _state.Close();
        }
    }
}