﻿using Ddd.Workshop.Finance.DomainModel.Aggregate.Account;
using Ddd.Workshop.Finance.Framework.Repository.Generic;

namespace Ddd.Workshop.Finance.DomainModel.Repository
{
    public interface IAccountRepository : ICrudRepository<Account, long>
    {
    }
}