﻿using Ddd.Workshop.Finance.DomainModel.Aggregate.Customer;
using Ddd.Workshop.Finance.Framework.Repository.Generic;

namespace Ddd.Workshop.Finance.DomainModel.Repository
{
    public interface ICustomerRepository : ICrudRepository<Customer, long>
    {
    }
}