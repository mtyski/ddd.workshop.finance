﻿using Ddd.Workshop.Finance.DomainModel.Aggregate.Customer;
using Ddd.Workshop.Finance.DomainModel.Aggregate.Customer.Exceptions;

namespace Ddd.Workshop.Finance.DomainModel.Factory
{
    public class CustomerFactory
    {
        public Customer CreateCustomer(
            string firstName,
            string surname,
            string middleName = null,
            string peselNumber = null)
        {
            if (string.IsNullOrWhiteSpace(firstName))
                throw new CustomerCannotHaveNullOrWhiteSpaceNameException();

            if (string.IsNullOrWhiteSpace(surname))
                throw new CustomerCannotHaveNullOrWhiteSpaceSurnameException();

            return new Customer(firstName, surname, middleName, peselNumber);
        }
    }
}