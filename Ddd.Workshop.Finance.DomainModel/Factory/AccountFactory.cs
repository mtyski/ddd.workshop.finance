﻿using Ddd.Workshop.Finance.DomainModel.Aggregate.Account;

namespace Ddd.Workshop.Finance.DomainModel.Factory
{
    public class AccountFactory
    {
        public Account CreateAccount() => new Account();
    }
}