﻿using Ddd.Workshop.Finance.DomainModel.Aggregate.Customer;
using Ddd.Workshop.Finance.DomainModel.DomainEvents.Account;
using Ddd.Workshop.Finance.DomainModel.Repository;
using Ddd.Workshop.Finance.DomainModel.Saga.Account.Exceptions;
using Ddd.Workshop.Finance.Framework.Saga;

namespace Ddd.Workshop.Finance.DomainModel.Saga.Account
{
    public class NewAccountRegisteredSaga : IEventListener<NewAccountRegistered>
    {
        private readonly ICustomerRepository _customerRepository;

        private readonly IAccountRepository _accountRepository;

        public NewAccountRegisteredSaga(ICustomerRepository customerRepository, IAccountRepository accountRepository)
        {
            _customerRepository = customerRepository;
            _accountRepository = accountRepository;
        }

        public void Handle(NewAccountRegistered domainEvent)
        {
            if (!(_customerRepository.Get(domainEvent.CustomerId) is Customer customer))
                throw new CustomerNotFoundException();

            if (!(_accountRepository.Get(domainEvent.AccountId) is Aggregate.Account.Account account))
                throw new AccountNotFoundException();

            customer.AssignAccount(domainEvent.AccountId);
            _customerRepository.Update(customer);
            account.Activate();
            _accountRepository.Update(account);
        }
    }
}