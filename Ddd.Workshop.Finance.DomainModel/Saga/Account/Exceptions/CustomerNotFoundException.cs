﻿using System;

namespace Ddd.Workshop.Finance.DomainModel.Saga.Account.Exceptions
{
    public class CustomerNotFoundException : Exception
    {
    }
}