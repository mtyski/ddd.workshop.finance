﻿using Ddd.Workshop.Finance.DomainModel.Repository;

namespace Ddd.Workshop.Finance.DomainModel.DomainService
{
    public class AccountClosureService
    {
        private readonly IAccountRepository _accountRepository;

        public AccountClosureService(IAccountRepository accountRepository) => _accountRepository = accountRepository;

        public void CloseAccount(long accountId)
        {
            var account = _accountRepository.Get(accountId);
            account.Close();
            _accountRepository.Update(account);
        }
    }
}