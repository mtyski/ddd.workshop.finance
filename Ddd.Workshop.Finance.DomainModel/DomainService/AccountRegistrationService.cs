﻿using Ddd.Workshop.Finance.DomainModel.Factory;
using Ddd.Workshop.Finance.DomainModel.Repository;

namespace Ddd.Workshop.Finance.DomainModel.DomainService
{
    public class AccountRegistrationService
    {
        private readonly IAccountRepository _accountRepository;

        private readonly AccountFactory _accountFactory;

        public AccountRegistrationService(IAccountRepository accountRepository, AccountFactory accountFactory)
        {
            _accountRepository = accountRepository;
            _accountFactory = accountFactory;
        }

        public void RegisterAccountForCustomer(long customerId)
        {
            var newAccount = _accountFactory.CreateAccount();
            newAccount.AssignToCustomer(customerId);
            _accountRepository.Save(newAccount);
        }
    }
}