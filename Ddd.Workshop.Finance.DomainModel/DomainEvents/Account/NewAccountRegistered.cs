﻿using System;

using Ddd.Workshop.Finance.Framework.DomainEvent;

namespace Ddd.Workshop.Finance.DomainModel.DomainEvents.Account
{
    public class NewAccountRegistered : IDomainEvent
    {
        public NewAccountRegistered(long accountId, long customerId)
        {
            AccountId = accountId;
            CustomerId = customerId;
            CreatedOn = DateTime.Now;
        }

        public long AccountId { get; }

        public long CustomerId { get; }

        public DateTime CreatedOn { get; }
    }
}