﻿using System;

using Ddd.Workshop.Finance.Framework.DomainEvent;

namespace Ddd.Workshop.Finance.DomainModel.DomainEvents.Account
{
    public class AccountClosed : IDomainEvent
    {
        public AccountClosed(long accountId)
        {
            AccountId = accountId;
            CreatedOn = DateTime.Now;
        }

        public long AccountId { get; }

        public DateTime CreatedOn { get; }
    }
}