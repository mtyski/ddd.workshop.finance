﻿using Ddd.Workshop.Finance.DomainModel.Entity.Account.Abstract;
using Ddd.Workshop.Finance.DomainModel.Entity.Account.Data;
using Ddd.Workshop.Finance.DomainModel.ValueObject.Transaction;

namespace Ddd.Workshop.Finance.DomainModel.Entity.Account.Implementation
{
    internal class Withdrawal : Transaction
    {
        public Withdrawal(Amount amount)
            : base(amount)
        {
        }

        public override TransactionData ToTransactionData() => new TransactionData(Id, Amount.ToDecimal(), TransactionData.TransactionType.Withdrawal);
    }
}