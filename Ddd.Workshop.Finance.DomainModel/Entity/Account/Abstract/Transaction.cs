﻿using System;

using Ddd.Workshop.Finance.DomainModel.Entity.Account.Data;
using Ddd.Workshop.Finance.DomainModel.ValueObject.Transaction;
using Ddd.Workshop.Finance.Framework.Entity;

namespace Ddd.Workshop.Finance.DomainModel.Entity.Account.Abstract
{
    internal abstract class Transaction : Entity<Guid>
    {
        protected Transaction(Amount amount) => Amount = amount;

        protected Amount Amount { get; }

        public abstract TransactionData ToTransactionData();

        public override bool Equals(object obj) => obj is Transaction transaction && transaction.Id.Equals(Id);

        public override int GetHashCode()
        {
            unchecked
            {
                return typeof(Transaction).GetHashCode() * Id.GetHashCode();
            }
        }
    }
}