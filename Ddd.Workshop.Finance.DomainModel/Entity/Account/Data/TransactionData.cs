﻿using System;

namespace Ddd.Workshop.Finance.DomainModel.Entity.Account.Data
{
    public class TransactionData
    {
        public TransactionData(Guid transactionId, decimal amount, TransactionType type)
        {
            TransactionId = transactionId;
            Amount = amount;
            Type = type;
        }

        public enum TransactionType
        {
            /// <summary>
            ///     Deposit (increases account balance).
            /// </summary>
            Deposit,

            /// <summary>
            ///     Withdrawal (decreases account balance).
            /// </summary>
            Withdrawal
        }

        public decimal Amount { get; }

        public TransactionType Type { get; }

        public Guid TransactionId { get; }
    }
}